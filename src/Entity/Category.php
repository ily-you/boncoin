<?php

namespace App\Entity;

use App\Repository\CategoryRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CategoryRepository::class)
 */
class Category
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Adverts", mappedBy="category")
     */
    private $adverts;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of adverts
     */ 
    public function getAdverts()
    {
        return $this->adverts;
    }

    /**
     * Set the value of adverts
     *
     * @return  self
     */ 
    public function setAdverts($adverts)
    {
        $this->adverts = $adverts;

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }
}
