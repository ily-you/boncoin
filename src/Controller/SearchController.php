<?php

namespace App\Controller;

use App\Form\SearchAdvertsType;
use App\Repository\AdvertsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;



class SearchController extends AbstractController {
    /**
     * @Route("/search", name="search_adverts")
     */
    public function search(Request $request, AdvertsRepository $advertsRepository){
      $adverts = [];

      $searchAdvertsForm = $this->createForm(SearchAdvertsType::class);

      if ($searchAdvertsForm->handleRequest($request)->isSubmitted() && $searchAdvertsForm->isValid()){
          $criteris = $searchAdvertsForm->getData();

          $adverts = $advertsRepository->searchAdverts($criteris);
     
      }
      return $this->render('adverts/search.html.twig', [
        'search_form' => $searchAdvertsForm->createView(),
        'adverts' => $adverts,
    ]);
    }

}