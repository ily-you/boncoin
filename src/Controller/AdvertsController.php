<?php

namespace App\Controller;

use App\Entity\Adverts;
use App\Form\AdvertType;
use App\Repository\CategoryRepository;
use App\Repository\RegionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AdvertsController extends AbstractController
{
    /**
     * @Route("/", name="advert_list")
     */
    public function index(Request $request, PaginatorInterface $paginator): Response
    {

        $donnees = $this->getDoctrine()->getRepository(Adverts::class)->findBy([],['created_at' => 'desc']);

        $adverts = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos articles)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            9 // Nombre de résultats par page
        );

        return $this->render('adverts/list.html.twig', ['adverts' => $adverts]);
    }

    /**
     * @Route("/add-advert", name="advert_add")
     * @param Request $request
     */
    public function add(Request $request, EntityManagerInterface $entityManager, ValidatorInterface $validator)
    {
        $advert = new Adverts();
        $advert->setUser($this->getUser());
        $advert->setPicture($request->request->get('picture'));

        $form = $this->createForm(AdvertType::class, $advert);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($advert);
            $entityManager->flush();

            return $this->redirectToRoute('gestion');
        }
        $errors = $validator->validate($advert);
        if (count($errors) > 0) {
            return new Response((string) $errors, 400);
        }

        return $this->render('adverts/add.html.twig', [

            'form' => $form->createView(),
        ]);
        
    }   

    /**
     * @Route("/advert/update/{id}", name="advert_show" )
     */
    public function update(int $id,RegionRepository $regionRepository, CategoryRepository $categoryRepository, Request $request,
     ValidatorInterface $validator): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $advert = $entityManager->getRepository(Adverts::class)->find($id);

        if (!$advert) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }

        $advert->setUser($this->getUser());
       // $advert->setPicture($request->request->get('picture'));

        $form = $this->createForm(AdvertType::class, $advert);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($advert);
            $entityManager->flush();

            return $this->redirectToRoute('gestion');
        }
        $errors = $validator->validate($advert);
        if (count($errors) > 0) {
            return new Response((string) $errors, 400);
        }
       
        return $this->render('adverts/update.html.twig', [

            'form' => $form->createView(),
        ]);
        $this->addFlash('success', 'Mise à jour efféctué.');
    }
    
    /**
     * @Route("/delete/{id}", name="advert_delete")
     * @return Response
     */

    public function delete(Adverts $adverts) {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($adverts);
        $entityManager->flush();
        $this->addFlash('success', 'Annonce supprimé.');
        //return new Response('annonce suprimée');
        return $this->redirectToRoute('gestion');
    }


}