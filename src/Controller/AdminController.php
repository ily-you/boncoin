<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Adverts;
use App\Entity\Adverts as EntityAdverts;
use App\Entity\User;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Knp\Component\Pager\PaginatorInterface;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     * @IsGranted("ROLE_ADMIN")
     */
    public function index(Request $request, PaginatorInterface $paginator): Response
    {

        $donnees = $this->getDoctrine()->getRepository(EntityAdverts::class)->findBy([],['created_at' => 'desc']);
        
        $adverts = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos articles)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            9 // Nombre de résultats par page
        );

        return $this->render('admin/showAdverts.html.twig', ['adverts' => $adverts]);
    }
      /**
     * @Route("/admin/users", name="admin_users")
     * @IsGranted("ROLE_ADMIN")
     */
    public function users(Request $request, PaginatorInterface $paginator): Response
    {

        $donnees = $this->getDoctrine()->getRepository(User::class)->findAll();
        
        $users = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos articles)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            9 // Nombre de résultats par page
        );

        return $this->render('admin/showUsers.html.twig', ['users' => $users]);
    }
    /**
     * @Route("/admin/delete/{id}", name="user_delete")
     * @IsGranted("ROLE_ADMIN")
     * @return Response
     */

    public function delete(User $user) {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($user);
        $entityManager->flush();

        return new Response('user suprimée');

    }
    /**
     * @Route("/admin/delete/{id}", name="advert_delete")
     * @IsGranted("ROLE_ADMIN")
     * @return Response
     */

    public function deleteAdvert(Adverts $adverts) {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($adverts);
        $entityManager->flush();

        return new Response('annonce suprimée');

    }
      /**
     * @Route("/admin/login", name="admin_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('admin/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/admin/logout", name="admin_logout")
     */
    public function logout(Request $request, PaginatorInterface $paginator): Response
    {
        $donnees = $this->getDoctrine()->getRepository(Adverts::class)->findBy([],['created_at' => 'desc']);

        $adverts = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos articles)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            9 // Nombre de résultats par page
        );

        return $this->render('adverts/list.html.twig', ['adverts' => $adverts]);
    }
}
