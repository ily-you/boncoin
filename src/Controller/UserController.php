<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Adverts;
use App\Repository\UserRepository;
use App\Repository\AdvertsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Request as HttpFoundationRequest;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Knp\Component\Pager\PaginatorInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/user", name="user")
     */
    public function index(): Response
    {
        return $this->render('user/index.html.twig', [
            'controller_name' => 'UserController',
        ]);
    }
    /**
     * @Route("/create-user", name="user_add")
     * @param Request $request
     * @param SessionInterface $session
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(HttpFoundationRequest $request, SessionInterface $session, EntityManagerInterface $entityManager, AuthenticationUtils $authenticationUtils)
    {
        if ($request->getMethod() == 'GET') {
            return $this->render('user/create-user.html.twig');
        }
        if ($request->getMethod() == 'POST') {
            $user = new User();
            $user->setEmail($request->request->get('email'));
            $user->setPhone($request->request->get('phone'));
            $user->setPassword(password_hash($request->request->get('password'), PASSWORD_BCRYPT));

            $entityManager->persist($user);
            $entityManager->flush();

            // stocker l'user en session pour savoir s'il est connecté ou non
            $session->set('user', $user);
            $error = $authenticationUtils->getLastAuthenticationError();
            // last username entered by the user
            $lastUsername = $authenticationUtils->getLastUsername();
            $this->addFlash('success', 'Bienvenue parmis nous.');
            return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
        }
    }
     
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="user_logout")
     */
    public function logout(Request $request, PaginatorInterface $paginator): Response
    {
        $donnees = $this->getDoctrine()->getRepository(Adverts::class)->findBy([],['created_at' => 'desc']);

        $adverts = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos articles)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            9 // Nombre de résultats par page
        );

        return $this->render('adverts/list.html.twig', ['adverts' => $adverts]);
    }

     /**
     *@Route("/advert", name="gestion")
     *@param $id
     *@param AdvertsRepository $advertsRepository
     */
    public function gestion( AdvertsRepository $advertsRepository)
    {
     $adverts = $advertsRepository->findBy(['user' => $this->getUser()]);
   
   
     return $this->render('adverts/show.html.twig', ['adverts' => $adverts]);

    }
}
