<?php

namespace App\Form;

use App\Entity\Region;
use App\Entity\Category;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class SearchAdvertsType extends AbstractType {
    const Price = [10,50,75,100,500,1000,2000,3000,5000,10000,25000,50000];
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('region', EntityType::class, [
            'class' => Region::class,
            'choice_label' => 'name' ,
            'attr' =>  ['class' => 'sel'],
        ])
        ->add('category', EntityType::class, [
            'class' => Category::class,
            'choice_label' => 'name'
        ])
        ->add('minPrice', ChoiceType::class, [
            'label' => 'Prix minimum',
            'choices' => array_combine(self::Price, self::Price)        
        ])
        ->add('maxPrice', ChoiceType::class, [
            'label' => 'Prix maximum',
            'choices' => array_combine(self::Price, self::Price)        
        ])
        ;
    }

}