<?php

namespace App\Repository;

use App\Entity\Adverts;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Adverts|null find($id, $lockMode = null, $lockVersion = null)
 * @method Adverts|null findOneBy(array $criteris, array $orderBy = null)
 * @method Adverts[]    findAll()
 * @method Adverts[]    findBy(array $criteris, array $orderBy = null, $limit = null, $offset = null)
 */
class AdvertsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Adverts::class);
    }

    // /**
    //  * @return Adverts[] Returns an array of Adverts objects
    //  */

    public function searchAdverts($criteris)
    {
        return $this->createQueryBuilder('c')
            ->innerJoin('c.region' , 'region')
            ->innerJoin('c.category' , 'category')
            ->where('region.name = :regionName')
            ->setParameter('regionName', $criteris['region']->getName())
            ->andWhere('category.name = :categoryName')
            ->setParameter('categoryName', $criteris['category']->getName())
            ->andWhere('c.price  > :minPrice')
            ->setParameter('minPrice', $criteris['minPrice'])
            ->andWhere('c.price  < :maxPrice')
            ->setParameter('maxPrice', $criteris['maxPrice'])
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }


    /*
    public function findOneBySomeField($value): ?Adverts
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
