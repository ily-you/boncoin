import { TomSelect } from '@orchidjs/sifter';
new TomSelect(".sel", {
    persist: false,
    createOnBlur: true,
    create: true
});