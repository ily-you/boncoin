import { Selektor } from '@selektor/selektor';

// If you want to init existing selektor by data attribute.
Selektor.scanHTMLSection();

try {
    //const select = new Selektor('#sel');
    const select = new Selektor(document.querySelector('.sel'));
    const select1 = new Selektor(document.querySelector('.sel1'));
} catch (e) {
    console.log('Select not found !');
}